FROM ubuntu:22.04

# Do not edit this values, copy .env-examples in .env and change values there
ENV OVERSEER_USER="USER"
ENV OVERSEER_PASSWORD="SECRETPASSWORD"
ENV OVERSEER_HOST="http://server-url:5500"
ENV LIBREFLOW_PROJECT="PROJECT"
ENV LIBREFLOW_SITE="SITE_NAME"
ENV PROJECT_ROOT_FOLDER="path/to/root"
ENV USER_GID=1000
ENV USER_UID=1000

RUN apt-get update && apt-get install python3 python3-pip ffmpeg -y

RUN groupadd -g $USER_GID -o lfsgrp
RUN useradd -m -u $USER_UID -g $USER_GID -o lfs

USER lfs
WORKDIR /home/lfs

RUN pip3 install libreflow.flows

# Variable requiered for the docker, do not change
ENV ROOT_DIR=/home/lfs/project_folder

COPY get_project.py .
COPY start_python.sh .

RUN mkdir /home/lfs/project_folder

ENTRYPOINT ["/bin/sh", "start_python.sh"]n
