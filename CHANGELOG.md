# Changelog


All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)[^1].

<!---
Types of changes

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

-->

## [Unreleased]

## [1.0.3] - 2024-06-05

### Added

* The user lfs is now by default UID=1000 and GID=1000. That allows better control over rights to write on mounted volume. Ids can be changed in the .env using the variables `USER_GID` and `USER_UID`

## [1.0.2] - 2024-06-05

### Changed

* The make and run will now restart the docker in case of failure. Usefull when db connection is lost for example

## [1.0.1] - 2024-06-04

### Changed

* Added a security check where the image looks for a .LibreflowSyncReady hidden file at the root of the folder to ensure the mount was done correctly. Note : do never create that file in the docker, it might think it's mouted properly when it's not. Create it by hand and ensure it's in the right place.

## [1.0.0] - 2024-05-31

### Added

* Initial release