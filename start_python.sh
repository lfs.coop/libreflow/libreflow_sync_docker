
echo ":: CONNECTING TO OVERSEER ::"

python3 get_project.py

echo ":: SETTING VARIABLES ::"
. ./source.env

if [ -z ${REDIS_URL+x} ]; then
    echo "ERROR : var REDIS_URL is unset (probably the rest of variables too) : stopping Now";
else
    echo ":: STARTING LIBREFLOW ::"
    python3 -m libreflow.sync --host $REDIS_URL --port $REDIS_PORT --db $REDIS_DB --cluster $REDIS_CLUSTER --project $PROJECT --site $SITE --session $SESSION --password $REDIS_PASSWORD 
fi