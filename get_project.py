import requests 
import os


_stop = False
user = os.environ.get("OVERSEER_USER", None)

password = os.environ.get("OVERSEER_PASSWORD", None)

overseer_host = os.environ.get("OVERSEER_HOST", None)

project = os.environ.get("LIBREFLOW_PROJECT", None)

site = os.environ.get("LIBREFLOW_SITE", None)

root_dir = os.environ.get("ROOT_DIR", None)

if not user:
    print("User NOT provided ! Will stop shortly")
if not password:
    print("password NOT provided ! Will stop shortly")
if not overseer_host:
    print("Overseer Host NOT provided ! Will stop shortly")
if not project:
    print("project NOT provided ! Will stop shortly")
if not site:
    print("Site NOT provided ! Will stop shortly")
if not root_dir:
    print("ROOT DIR not provided")

if not user or not password or not overseer_host or not project or not site or not root_dir:
    _stop = True


if not os.path.exists(root_dir):
    print("ERROR : Root dir {root_dir} does not exists in the docker")
    _stop = True
elif not os.path.exists(os.path.join(root_dir, ".LibreflowSyncReady")):
    print("ERROR : Root dir {root_dir} is not ready for Sync? Missing .LibreflowSyncReady")
    _stop = True

if _stop:
    print("ERROR : process was not complete, will stop now")
    exit()


class Controler():
    BASE_URL = 'http://0.0.0.0:5500'
    CONFIG = {'headers': None}

    def __init__(self, overseer):
        self.BASE_URL = overseer
        self.current_user = None


    def get_token(self, data: dict) -> str:
        try:
            resp = requests.post(
                self.BASE_URL + '/token', data=data
            )
            resp.raise_for_status()
        except requests.exceptions.HTTPError as err:
            return resp.json()
        except requests.exceptions.RequestException as err:
            print(f'ERROR: {err}')
            return None
        else:
            return resp.json()

    def api_get(self, route, json):
        try:
            resp = requests.get(
                self.BASE_URL + route, json=json,
                headers=self.CONFIG['headers']
            )
            resp.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print("ERROR", err)
            return resp.json()
        except requests.exceptions.RequestException as err:
            print(f'ERROR: {err}')
            return None
        else:
            return resp.json()

    def log_in(self, login, password):
        token = self.get_token({'username': login, 'password': password})

        if token is None:
            print(f'ERROR: Connection failed to server, {self.BASE_URL}')
            return False
        elif 'detail' in token and token['detail'] == "invalid-auth":
            print(f'ERROR: Authentification invalid to server, {self.BASE_URL}')
            return False

        self.CONFIG['headers'] = {
            'Authorization': 'Bearer {}'.format(token['access_token'])
        }
        self.current_user=login
        return True

    def get_project(self, project, site):
        data_resolve = self.api_get(f'/project-resolve?project={project}&site={site}&user={self.current_user}', json={})

        if not data_resolve:
            print(f"ERROR : no project returned for site '{site}', user '{self.current_user}' and project '{project}'")

        return(data_resolve)

_o = Controler(overseer_host)
c = _o.log_in(user, password)


project = _o.get_project(project, site)

if not project:
    print("ERROR PROJECT WAS NOT LOADED")
    exit()


source = ""
print("Setting environment variables")


source += f'export REDIS_URL={project["redis_url"]}\n'
source += f'export REDIS_PORT={project["redis_port"]}\n'
source += f'export REDIS_DB={project["redis_db"]}\n'
source += f'export REDIS_CLUSTER={project["redis_cluster"]}\n'
source += f'export PROJECT={project["name"].lower()}\n' # THIS SHOULD NOT BE LIKE THAT, BUT ANPO IS UPERSCALE IN OVERSEER #FIXME
source += f'export SITE={site}\n'
source += f'export SESSION="lfs"\n'
source += f'export REDIS_PASSWORD={project["redis_password"]}\n'

print("saving in: ", os.path.join(os.path.dirname(__file__), "source.env"))
f = open(os.path.join(os.path.dirname(__file__), "source.env"), "w")
f.write(source)
f.close()
