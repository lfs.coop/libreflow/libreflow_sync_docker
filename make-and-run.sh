#!/bin/sh


echo "==============="
echo "LOADING VARIABLES: ${IMAGE_NAME}"
echo "==============="
if [ -f ./.env ]; then
  echo "- sourcing: '.env'"
  . ./.env
else
    echo ".env DOES NOT EXISTS : STOPPING NOW"
    exit
fi

echo "Checking ROOT FOLDER..."
echo $PROJECT_ROOT_FOLDER
if [ ! -d "$PROJECT_ROOT_FOLDER" ]; then
    echo "         ERROR ! ROOT FOLDER $DIRECTORY does not exist !"
    exit
else
  if [ ! -f "$PROJECT_ROOT_FOLDER/.LibreflowSyncReady" ]; then
    echo "         ERROR ! ROOT FOLDER $DIRECTORY is not ready for sync?     :::: Missing .LibreflowSyncReady "
    exit
  else
    echo "                 ... OK"
  fi
fi

echo "==============="
echo "BUILDING IMAGE: ${IMAGE_NAME}"
echo "==============="

docker build --tag libreflowsync/app .


echo "==============="
echo "DELETING PREVIOUS DOCKER:"
echo "==============="

APP_CONTAINER_NAME=libreflow_sync
if [ "$(docker ps -aq -f name=^${APP_CONTAINER_NAME}$)" ]; then
  docker rm -f $(docker ps -aq -f name=^${APP_CONTAINER_NAME}$)
fi


echo "==============="
echo "RUNNING DOCKER:"
echo "==============="


docker run \
  -d \
  -e "OVERSEER_USER=$OVERSEER_USER" \
  -e "OVERSEER_PASSWORD=$OVERSEER_PASSWORD" \
  -e "OVERSEER_HOST=$OVERSEER_HOST" \
  -e "LIBREFLOW_PROJECT=$LIBREFLOW_PROJECT" \
  -e "LIBREFLOW_SITE=$LIBREFLOW_SITE" \
  --name ${APP_CONTAINER_NAME} \
  -v $PROJECT_ROOT_FOLDER:/home/lfs/project_folder \
  --restart always \
  libreflowsync/app 
