# Libreflow sync

Libreflow sync docker is a simple libreflow docker, without extensions, that can be used just for automaticaly syncking content for a specific project and specific site. 

## Requierements 

It requieres the host to **get access to the root folder of the project** as shared in a site (studio for example) and a valid user accessing the Overseer service for the project. The mount point on the host is not necesserly the one used by the artists as it overriden by the variable `PROJECT_ROOT_FOLDER`.
**This dockerfile also requieres a Overseer Service**, so you provide an user that fetch the last configuration rather than providing all the infos (such as db user/location/password, ...)

## Installing
You need to copy the `.env-example` file in `.env` and fill the requiered info you can get from your admins.

## Setuping the root folder
Once setup correctly the mount folder, you won't be able to start the image untill you create the hidden file .LibreflowSyncReady at the root of the project. That is where the lib folder is or the movie/episode folder is. This is from that very same folder that everything will be synced, to take care.

## Testing
Test with another site than your site (for example in a LFS project we usualy have the `faraway` site) so you don't mess with your real site sync status.

